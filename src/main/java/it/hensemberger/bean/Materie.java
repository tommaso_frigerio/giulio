package it.hensemberger.bean;



public class Materie  {

	private Integer id;
	protected String nome;
	protected String descrizione;
	protected String lab;
	public Materie(Integer id, String nome, String descrizione, String lab) {
		
		this.id = id;
		this.nome = nome;
		this.descrizione = descrizione;
		this.lab = lab;
	}
	public Materie() {
		// TODO Auto-generated constructor stub
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getLab() {
		return lab;
	}
	public void setLab(String lab) {
		this.lab = lab;
	}
	
	
	
	
	
}
