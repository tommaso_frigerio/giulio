package it.hensemberger.bean;

import java.util.List;

public class Classe {
	private Integer indirizzo;
	protected List<Alunni> alunni = null;
	protected String id;
	public Classe(Integer indirizzo, List<Alunni> alunni, String id) {
		
		this.indirizzo = indirizzo;
		this.alunni = alunni;
		this.id = id;
	}
	
	public Classe() {
		// TODO Auto-generated constructor stub
	}
	public Integer getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(Integer indirizzo){
		this.indirizzo = indirizzo;
	}
	
	
	public List<Alunni> getAlunni() {
		if(this.alunni!=null)
		{
			this.alunni=getAlunni();
		}
		return alunni;
	}
	public void setAlunni(List<Alunni> alunni) {
		this.alunni = alunni;
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
	
	

}
