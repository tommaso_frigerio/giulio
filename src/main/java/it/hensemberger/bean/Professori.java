package it.hensemberger.bean;

import java.sql.Date;

public class Professori {
	private Integer matricola;
	protected String nome;
	protected String cognome;
	protected String pass;
	protected Date data_nascita;
	public Professori(Integer matricola, String nome, String cognome, String pass, Date data_nascita) {
		
		this.matricola = matricola;
		this.nome = nome;
		this.cognome = cognome;
		this.pass = pass;
		this.data_nascita = data_nascita;
	}
	public Professori() {
		// TODO Auto-generated constructor stub
	}
	public Integer getMatricola() {
		return matricola;
	}
	public void setMatricola(Integer matricola) {
		this.matricola = matricola;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public Date getData_nascita() {
		return data_nascita;
	}
	public void setData_nascita(Date data_nascita) {
		this.data_nascita = data_nascita;
	}
	
	
}
