package it.hensemberger.bean;



import java.util.Date;
import java.util.List;

public class Alunni {
	
	
	
	private Integer matricola;
	protected String idclasse;
	protected String nome;
	protected String cognome;
	protected Date data_nascita;
	protected String pass;
	
	public Alunni(Integer matricola, String idclasse, String nome, String cognome, Date data_nascita, String pass) 
	{
	
		this.matricola = matricola;
		this.idclasse = idclasse;
		this.nome = nome;
		this.cognome = cognome;
		this.data_nascita = data_nascita;
		this.pass = pass;
	
	}
	
	public Alunni() {
		// TODO Auto-generated constructor stub
	}

	public Integer getMatricola() {
		return matricola;
	}

	public void setMatricola(Integer matricola) {
		this.matricola = matricola;
	}

	public String getIdclasse() {
		return idclasse;
	}

	public void setIdclasse(String idclasse) {
		this.idclasse = idclasse;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public Date getData_nascita() {
		return data_nascita;
	}

	public void setData_nascita(Date data_nascita) {
		this.data_nascita = data_nascita;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

}

	
	
	
	