package it.hensemberger.bean;

import java.util.List;

public class Commenti {

	private Integer id;
	protected String commento;
	protected Integer id_valutazione;
	protected Integer matricola_stud;
	protected Integer commento_padre;
	public Commenti(Integer id, String commento, Integer id_valutazione, Integer matricola_stud,
	Integer commento_padre) {

		this.id = id;
		this.commento = commento;
		this.id_valutazione = id_valutazione;
		this.matricola_stud = matricola_stud;
		this.commento_padre = commento_padre;
	}
	public Commenti() {

	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCommento() {
		return commento;
	}
	public void setCommento(String commento) {
		this.commento = commento;
	}
	public Integer getId_valutazione() {
		return id_valutazione;
	}
	public void setId_valutazione(Integer id_valutazione) {
		this.id_valutazione = id_valutazione;
	}
	public Integer getMatricola_stud() {
		return matricola_stud;
	}
	public void setMatricola_stud(Integer matricola_stud) {
		this.matricola_stud = matricola_stud;
	}
	public Integer getCommento_padre() {
		return commento_padre;
	}
	public void setCommento_padre(Integer commento_padre) {
		this.commento_padre = commento_padre;
	} 
	
	
	
}
