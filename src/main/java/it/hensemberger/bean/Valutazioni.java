package it.hensemberger.bean;

public class Valutazioni {
	private Integer id;
	protected float valutazione;
	protected Integer id_materie_classi;
	protected Integer matricola_stud;
	public Valutazioni(Integer id, float valutazione, Integer id_materie_classi, Integer matricola_stud) {

		this.id = id;
		this.valutazione = valutazione;
		this.id_materie_classi = id_materie_classi;
		this.matricola_stud = matricola_stud;
	}
	
	public Valutazioni() {
		// TODO Auto-generated constructor stub
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public float getValutazione() {
		return valutazione;
	}
	public void setValutazione(float valutazione) {
		this.valutazione = valutazione;
	}
	public Integer getId_materie_classi() {
		return id_materie_classi;
	}
	public void setId_materie_classi(Integer id_materie_classi) {
		this.id_materie_classi = id_materie_classi;
	}
	public Integer getMatricola_stud() {
		return matricola_stud;
	}
	public void setMatricola_stud(Integer matricola_stud) {
		this.matricola_stud = matricola_stud;
	}

	
	
}
