package it.hensemberger.bean;

import java.sql.Time;

public class MaterieClasse extends Materie {
	
	

	private Integer idesteso;
	protected String classe;
	protected Integer matrprofe;
	protected Integer idmateria;
	protected Integer ore;
	
	public MaterieClasse(Integer id, String nome, String descrizione, String lab, Integer idesteso,
			String classe,
			Integer matrprofe, Integer idmateria, Integer ore) {
		super(id, nome, descrizione, lab);
		this.idesteso = id;
		this.classe = classe;
		this.matrprofe = matrprofe;
		this.idmateria = idmateria;
		this.ore = ore;
	}
	public MaterieClasse() {
		// TODO Auto-generated constructor stub
	}
	public Integer getIdesteso() {
		return idesteso;
	}
	public void setIdesteso(Integer idesteso) {
		this.idesteso = idesteso;
	}
	public String getClasse() {
		return classe;
	}
	public void setClasse(String classe) {
		this.classe = classe;
	}
	public Integer getMatrprofe() {
		return matrprofe;
	}
	public void setMatrprofe(Integer matrprofe) {
		this.matrprofe = matrprofe;
	}
	public Integer getIdmateria() {
		return idmateria;
	}
	public void setIdmateria(Integer idmateria) {
		this.idmateria = idmateria;
	}
	public Integer getOre() {
		return ore;
	}
	public void setOre(Integer ore) {
		this.ore = ore;
	}
 

	
	
	
	
}
