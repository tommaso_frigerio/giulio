package it.hensemberger.bean;

import java.util.List;

public class Indirizzo {
	
	private Integer id;
	protected String nome;
	protected String desc;
	List<Classe> classe=null;
	
	public Indirizzo(Integer id, String nome, String desc) {

		this.id = id;
		this.nome = nome;
		this.desc = desc;
	}
	
	
	public Indirizzo() {
		// TODO Auto-generated constructor stub
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getDesc() {
		return desc;
	}


	public void setDesc(String desc) {
		this.desc = desc;
	}


	public List<Classe> getClasse() {
		return classe;
	}


	public void setClasse(List<Classe> classe) {
		this.classe = classe;
	}



}
