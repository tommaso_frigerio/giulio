package it.hensemberger.dao;
   


import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.hensemberger.utility.DBUtils;
import it.hensemberger.bean.Materie;
import it.hensemberger.bean.Valutazioni;

public class DaoValutazioni {

	private static String QUERY_LIST="select * from Valutazioni";
	private static String QUERY="select * from Valutazioni where id = ? ";
	private static String QUERY_INSERT="INSERT INTO Valutazioni (valutazione,id_materie_classi,matricola_stud) VALUES (?,?,?)";
	private static String QUERY_UPDATE="update Valutazioni SET  valutazione=?,id_materie_classi=?,matricola_stud=? WHERE id=?";
	private static String QUERY_SPECIFIC="select * from Valutazioni where id_materie_classi=? ";
	
	public List<Valutazioni> getListValutazioni() throws SQLException
	{
		Valutazioni val;
		List<Valutazioni> Listid=new ArrayList<Valutazioni>();
		Integer t;
		float temp;
	
		
		
		
		
		Connection connection=DBUtils.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement(QUERY_LIST);
		ResultSet resultSet2= preparedStatement.executeQuery();
		while(resultSet2.next())
		{
			val=new Valutazioni();
			t=resultSet2.getInt("id");
			val.setId(t);
			temp=resultSet2.getFloat("valutazione");
			val.setValutazione(temp);
			t=resultSet2.getInt("id_materie_classi");
			val.setId_materie_classi(t);
			t=resultSet2.getInt("matricola_stud");
			val.setMatricola_stud(t);

			Listid.add(val);
			
			
			
		}
 
		return Listid;
		
	}
	
	
	public Valutazioni getValutazione(Integer id) throws SQLException
	{
		Valutazioni val= new Valutazioni();
		Integer t;
		float temp;
		
		
		
		
		Connection connection=DBUtils.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement(QUERY);
		preparedStatement.setInt(1,id);
		ResultSet resultSet2= preparedStatement.executeQuery();
		if(resultSet2.next())
		{
			
			t=resultSet2.getInt("id");
			val.setId(t);
			temp=resultSet2.getFloat("valutazione");
			val.setValutazione(temp);
			t=resultSet2.getInt("id_materie_classi");
			val.setId_materie_classi(t);
			t=resultSet2.getInt("matricola_stud");
			val.setMatricola_stud(t);
			
		}
 
		return val;
		
	}
	
	public Valutazioni addValutazioni(Valutazioni v) throws SQLException
	{

	Valutazioni val = null;
	Integer t;

	String temp;


	//if(alreadyExist(m))

	Connection connection=DBUtils.getConnection();
	PreparedStatement preparedStatement = connection.prepareStatement(QUERY_INSERT,PreparedStatement.RETURN_GENERATED_KEYS);
	
	preparedStatement.setFloat(1,v.getValutazione());
	preparedStatement.setInt(2,v.getId_materie_classi());
	preparedStatement.setInt(3,v.getMatricola_stud());

	Integer rownum = preparedStatement.executeUpdate();

	Integer id = 0;
	ResultSet rs = preparedStatement.getGeneratedKeys();
	if( rownum != 0 && rs.next()) {
	      id = rs.getInt(1);
	      val=getValutazione(id);
	}


	return val;


	}
	 
	public Valutazioni updateValutazioni(Valutazioni v) throws SQLException
	{
	Valutazioni val=new Valutazioni();
	Integer t;
	String temp;


	if(v==null)
		return null;

	Connection connection=DBUtils.getConnection();
	PreparedStatement preparedStatement = connection.prepareStatement(QUERY_UPDATE,PreparedStatement.RETURN_GENERATED_KEYS);

	preparedStatement.setFloat(1,v.getValutazione());
	preparedStatement.setInt(2,v.getId_materie_classi());
	preparedStatement.setInt(3,v.getMatricola_stud());
	preparedStatement.setInt(4, v.getId());
	Integer rownum = preparedStatement.executeUpdate();


	val=getValutazione(v.getId());

	return val;	
	}




	public Valutazioni delete(int idval) throws SQLException{
	return null;
	}

	protected Boolean alreadyExist(Valutazioni v) throws SQLException
	{
	Connection connection=DBUtils.getConnection();
	PreparedStatement preparedStatement = connection.prepareStatement(QUERY_SPECIFIC);
	preparedStatement.setInt(1,v.getId_materie_classi());

	ResultSet rs= preparedStatement.executeQuery();

	if(rs.next())
	            return true;
	        else 
	            return false;
	}
	}







