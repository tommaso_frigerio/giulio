package it.hensemberger.dao;
   


import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.hensemberger.utility.DBUtils;
import it.hensemberger.bean.Materie;
import it.hensemberger.bean.Professori;

public class DaoProfessori {

	private static String QUERY_LIST="select * from Professori";
	private static String QUERY="select * from Professori where matricola = ? ";
	private static String QUERY_INSERT="INSERT INTO Professori ( nome,cognome,pass,data_nascita) VALUES (?,?,?,?)";
	private static String QUERY_UPDATE="update Professori SET nome=?,cognome=?,pass=?,data_nascita=? WHERE matricola=?";
	private static String QUERY_SPECIFIC="select * from Professori where cognome=? AND nome=? ";
	
	
	public List<Professori> getListProfessori() throws SQLException
	{
		Professori prof;
		List<Professori> Listmatr=new ArrayList<Professori>();
		Integer t;
		String temp;
		Date data;
		
		
		
		
		Connection connection=DBUtils.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement(QUERY_LIST);
		ResultSet resultSet2= preparedStatement.executeQuery();
		while(resultSet2.next())
		{
			prof=new Professori();
			t=resultSet2.getInt("matricola");
			prof.setMatricola(t);
			temp=resultSet2.getString("nome");
			prof.setNome(temp);
			temp=resultSet2.getString("cognome");
			prof.setCognome(temp);
			temp=resultSet2.getString("pass");
			prof.setPass(temp);
			data=resultSet2.getDate("data_nascita");
			prof.setData_nascita(data);
			Listmatr.add(prof);
			
			
			
		}
 
		return Listmatr;
		
	}
	
	
	public Professori getProfessore(Integer id) throws SQLException
	{
		Professori prof= new Professori();
		Integer t;
		String temp;
		Date data;
		
		
		
		
		Connection connection=DBUtils.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement(QUERY);
		preparedStatement.setInt(1,id);
		ResultSet resultSet2= preparedStatement.executeQuery();
		if(resultSet2.next())
		{
			
			t=resultSet2.getInt("matricola");
			prof.setMatricola(t);
			temp=resultSet2.getString("nome");
			prof.setNome(temp);
			temp=resultSet2.getString("cognome");
			prof.setCognome(temp);
			temp=resultSet2.getString("pass");
			prof.setPass(temp);
			data=resultSet2.getDate("data_nascita");
			prof.setData_nascita(data);
			
			
			
			
		}
 
		return prof;
		
	}
	








public Professori addProfe(Professori p) throws SQLException
{

Professori prof = null;
Integer t;
Date data;
String temp;


//if(alreadyExist(m))

Connection connection=DBUtils.getConnection();
PreparedStatement preparedStatement = connection.prepareStatement(QUERY_INSERT,PreparedStatement.RETURN_GENERATED_KEYS);

preparedStatement.setString(1,p.getNome());
preparedStatement.setString(2,p.getCognome());
preparedStatement.setString(3,p.getPass());
preparedStatement.setDate(4,new java.sql.Date(p.getData_nascita().getTime()));

Integer rownum = preparedStatement.executeUpdate();

Integer matricola= 0;
ResultSet rs = preparedStatement.getGeneratedKeys();
if( rownum != 0 && rs.next()) {
      matricola = rs.getInt(1);
      prof=getProfessore(matricola);
}


return prof;


}
 
public Professori updateprofe(Professori p) throws SQLException
{
Professori prof=new Professori();
Integer t;
String temp;


if(p==null)
	return null;
Connection connection=DBUtils.getConnection();
PreparedStatement preparedStatement = connection.prepareStatement(QUERY_UPDATE,PreparedStatement.RETURN_GENERATED_KEYS);

preparedStatement.setString(1,p.getNome());
preparedStatement.setString(2,p.getCognome());
preparedStatement.setString(3,p.getPass());
preparedStatement.setTimestamp(4,new java.sql.Timestamp(p.getData_nascita().getTime()));
preparedStatement.setInt(5,p.getMatricola());
Integer rownum = preparedStatement.executeUpdate();


prof=getProfessore(p.getMatricola());

return prof;	
}




public Professori delete(int matrprof) throws SQLException{
return null;
}

protected Boolean alreadyExist(Professori p) throws SQLException
{
Connection connection=DBUtils.getConnection();
PreparedStatement preparedStatement = connection.prepareStatement(QUERY_SPECIFIC);

preparedStatement.setString(1,p.getNome());
preparedStatement.setString(2, p.getCognome());
ResultSet rs= preparedStatement.executeQuery();

if(rs.next())
            return true;
        else 
            return false;
}
}















