package it.hensemberger.dao;
   


import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.hensemberger.utility.DBUtils;

import it.hensemberger.bean.Materie;

public class DaoMaterie {

	private static String QUERY_LIST="select * from Materie";
	private static String QUERY="select * from Materie where id = ? ";
	private static String QUERY_INSERT="INSERT INTO Materie (nome,descrizione,laboratorio) VALUES (?,?,?)";
	private static String QUERY_UPDATE="update Materie SET nome=?,descrizione=?,laboratorio=? WHERE id=?";
	private static String QUERY_SPECIFIC="select * from Materie where id=?";
	
	public List<Materie> getListMaterie() throws SQLException
	{
		Materie mat;
		List<Materie> Listmatr=new ArrayList<Materie>();
		Integer t;
		String temp;
	
		
		
		
		
		Connection connection=DBUtils.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement(QUERY_LIST);
		ResultSet resultSet2= preparedStatement.executeQuery();
		while(resultSet2.next())
		{
			mat=new Materie();
			t=resultSet2.getInt("id");
			mat.setId(t);
			temp=resultSet2.getString("nome");
			mat.setNome(temp);
			temp=resultSet2.getString("descrizione");
			mat.setDescrizione(temp);
			temp=resultSet2.getString("laboratorio");
			mat.setLab(temp);

			Listmatr.add(mat);
			
			
			
		}
 
		return Listmatr;
		
	}
	
	public Materie getmaterie(Integer id) throws SQLException
	{
		Materie mat= new Materie();
		Integer t;
		String temp;
		
		
		
		
		Connection connection=DBUtils.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement(QUERY);
		preparedStatement.setInt(1,id);
		ResultSet resultSet2= preparedStatement.executeQuery();
		if(resultSet2.next())
		{
			
			t=resultSet2.getInt("id");
			mat.setId(t);
			temp=resultSet2.getString("nome");
			mat.setNome(temp);
			temp=resultSet2.getString("descrizione");
			mat.setDescrizione(temp);
			temp=resultSet2.getString("laboratorio");
			mat.setLab(temp);
			
			
		}
 
		return mat;
		
	}

public Materie addMaterie(Materie m) throws SQLException
{

Materie mat = null;
Integer t;

String temp;


//if(alreadyExist(m))

Connection connection=DBUtils.getConnection();
PreparedStatement preparedStatement = connection.prepareStatement(QUERY_INSERT,PreparedStatement.RETURN_GENERATED_KEYS);
preparedStatement.setString(1,m.getNome());
preparedStatement.setString(2,m.getDescrizione());
preparedStatement.setString(3,m.getLab());

Integer rownum = preparedStatement.executeUpdate();

Integer id = 0;
ResultSet rs = preparedStatement.getGeneratedKeys();
if( rownum != 0 && rs.next()) {
      id = rs.getInt(1);
      mat=getmaterie(id);
}


return mat;


}
 
public Materie updateMaterie(Materie m) throws SQLException
{
Materie mate=new Materie();
Integer t;

String temp;


if(m==null)
	return null;

Connection connection=DBUtils.getConnection();
PreparedStatement preparedStatement = connection.prepareStatement(QUERY_UPDATE,PreparedStatement.RETURN_GENERATED_KEYS);
preparedStatement.setString(1,m.getNome());
preparedStatement.setString(2,m.getDescrizione());
preparedStatement.setString(3,m.getLab());

preparedStatement.setInt(4, m.getId());

Integer rownum = preparedStatement.executeUpdate();


mate=getmaterie(m.getId());

return mate;	
}




public Materie delete(int idmat) throws SQLException{
return null;
}

protected Boolean alreadyExist(Materie m) throws SQLException
{
Connection connection=DBUtils.getConnection();
PreparedStatement preparedStatement = connection.prepareStatement(QUERY_SPECIFIC);

preparedStatement.setString(1,m.getNome());
ResultSet rs= preparedStatement.executeQuery();

if(rs.next())
            return true;
        else 
            return false;
}
}
















