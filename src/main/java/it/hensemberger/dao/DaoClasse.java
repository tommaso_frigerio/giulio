package it.hensemberger.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.hensemberger.utility.DBUtils;
import it.hensemberger.bean.Classe;
import it.hensemberger.bean.Indirizzo;


public class DaoClasse {

	private static String QUERY_LIST="select * from Classe";
	private static String QUERY="select * from Classe where id= ? ";
	private static String QUERY_INSERT="INSERT INTO Classe (id, id_indirizzo) VALUES (?,?)";
	private static String QUERY_UPDATE="update Classe SET id=?, id_indirizzo=? WHERE id=?";
	private static String QUERY_SPECIFIC="select * from Classe where id=? ";

	public List<Classe> getListClasse() throws SQLException
	{
		Classe cla;
		List<Classe> Listid=new ArrayList<Classe>();
		String temp;
		Integer t;
		

		
		
		
		Connection connection=DBUtils.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement(QUERY_LIST);
		ResultSet resultSet2= preparedStatement.executeQuery();
		while(resultSet2.next())
		{
			cla=new Classe();
			temp=resultSet2.getString("id");
			cla.setId(temp);
			t=resultSet2.getInt("id_indirizzo");
			cla.setIndirizzo(t);
			
			
			
			
			
			Listid.add(cla);
			
			
			
		}
 
		return Listid;
		
	}
	
	
	public Classe getClasse(Integer id) throws SQLException
	{
		Classe cla=new Classe();
	
		String temp;
		Integer t;
						
		Connection connection=DBUtils.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement(QUERY);
		preparedStatement.setInt(1, id);
		ResultSet resultSet2= preparedStatement.executeQuery();
		if(resultSet2.next())
		{
			temp=resultSet2.getString("id");
			cla.setId(temp);
			t=resultSet2.getInt("id_indirizzo");
			cla.setIndirizzo(t);
		}
 
		return cla;
		
	}
	




public Classe addcla(Classe c) throws SQLException
{

Classe cla= null;
Integer t;

String temp;


//if(alreadyExist(m))

Connection connection=DBUtils.getConnection();
PreparedStatement preparedStatement = connection.prepareStatement(QUERY_INSERT,PreparedStatement.RETURN_GENERATED_KEYS);
preparedStatement.setString(1,c.getId());
DaoIndirizzo ind = null;

Integer rownum = preparedStatement.executeUpdate();


Integer id = 0;
ResultSet rs = preparedStatement.getGeneratedKeys();
if( rownum != 0 && rs.next()) {
      id = rs.getInt(1);
      cla=getClasse(id);
}


return cla;


}
 
public Classe updateClasse(Classe c) throws SQLException
{
Classe cla=new Classe();
Integer t;
String temp;


if(c==null)
	return null;

Connection connection=DBUtils.getConnection();
PreparedStatement preparedStatement = connection.prepareStatement(QUERY_UPDATE,PreparedStatement.RETURN_GENERATED_KEYS);
preparedStatement.setString(1,c.getId());
DaoIndirizzo ind = null;
preparedStatement.setString(2,c.getId());
Integer rownum = preparedStatement.executeUpdate();


cla=getClasse(Integer.parseInt(c.getId()));

return cla;	
}




public Classe delete(int idcla) throws SQLException{
return null;
}

protected Boolean alreadyExist(Classe c) throws SQLException
{
Connection connection=DBUtils.getConnection();
PreparedStatement preparedStatement = connection.prepareStatement(QUERY_SPECIFIC);
preparedStatement.setString(1,c.getId());
ResultSet rs= preparedStatement.executeQuery();

if(rs.next())
            return true;
        else 
            return false;
}
}












