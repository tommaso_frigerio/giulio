package it.hensemberger.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.hensemberger.utility.DBUtils;
import it.hensemberger.bean.Indirizzo;
import it.hensemberger.bean.Materie;

public class DaoIndirizzo {

	private static String QUERY_LIST="select * from Indirizzo";
	private static String QUERY="select * from Indirizzo where id= ? ";
	private static String QUERY_INSERT="INSERT INTO Indirizzo (nome,descrizione) VALUES (?,?,?)";
	private static String QUERY_UPDATE="update Indirizzo SET nome=?,descrizione=? WHERE id=?";
	private static String QUERY_SPECIFIC="select * from Indirizzo where  nome=? ";
	
	public List<Indirizzo> getListIndirizzo() throws SQLException
	{
		Indirizzo Ind;
		List<Indirizzo> Listid=new ArrayList<Indirizzo>();
		Integer t;
		String temp;
		
		
		
		
		Connection connection=DBUtils.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement(QUERY_LIST);
		ResultSet resultSet2= preparedStatement.executeQuery();
		while(resultSet2.next())
		{
			Ind=new Indirizzo();
			t=resultSet2.getInt("id");
			Ind.setId(t);
			temp=resultSet2.getString("descrizione");
			Ind.setNome(temp);
			temp=resultSet2.getString("nome");
			Ind.setDesc(temp);
			Listid.add(Ind);
			
			
			
		}
 
		return Listid;
		
	}
	
	
	public Indirizzo getIndirizzo(Integer id) throws SQLException
	{
		Indirizzo Ind=new Indirizzo();
		Integer t;
		String temp;
		
		
		
		
		Connection connection=DBUtils.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement(QUERY);
		preparedStatement.setInt(1, id);
		ResultSet resultSet2= preparedStatement.executeQuery();
		if(resultSet2.next())
		{
			
			t=resultSet2.getInt("id");
			Ind.setId(t);
			temp=resultSet2.getString("descrizione");
			Ind.setNome(temp);
			temp=resultSet2.getString("nome");
			Ind.setDesc(temp);
			
			
			
			
		}
 
		return Ind;
		
	}
	





public Indirizzo addind(Indirizzo i) throws SQLException
{

Indirizzo ind = null;
Integer t;

String temp;


//if(alreadyExist(m))

Connection connection=DBUtils.getConnection();
PreparedStatement preparedStatement = connection.prepareStatement(QUERY_INSERT,PreparedStatement.RETURN_GENERATED_KEYS);
preparedStatement.setString(1,i.getNome());
preparedStatement.setString(2,i.getDesc());

Integer rownum = preparedStatement.executeUpdate();

Integer id = 0;
ResultSet rs = preparedStatement.getGeneratedKeys();
if( rownum != 0 && rs.next()) {
      id = rs.getInt(1);
      ind=getIndirizzo(id);
}


return ind;


}
 
public Indirizzo updateIndirizzo(Indirizzo i) throws SQLException
{
Indirizzo ind=new Indirizzo();
Integer t;
String temp;


if(i==null)
	return null;

Connection connection=DBUtils.getConnection();
PreparedStatement preparedStatement = connection.prepareStatement(QUERY_UPDATE,PreparedStatement.RETURN_GENERATED_KEYS);

preparedStatement.setString(1,i.getNome());
preparedStatement.setString(2,i.getDesc());

preparedStatement.setInt(3,i.getId());
Integer rownum = preparedStatement.executeUpdate();


ind=getIndirizzo(i.getId());

return ind;	
}




public Indirizzo delete(int idind) throws SQLException{
return null;
}

protected Boolean alreadyExist(Indirizzo i) throws SQLException
{
Connection connection=DBUtils.getConnection();
PreparedStatement preparedStatement = connection.prepareStatement(QUERY_SPECIFIC);
preparedStatement.setString(2,i.getNome());
ResultSet rs= preparedStatement.executeQuery();

if(rs.next())
            return true;
        else 
            return false;
}
}











