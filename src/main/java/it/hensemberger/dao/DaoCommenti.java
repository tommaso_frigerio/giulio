package it.hensemberger.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.hensemberger.utility.DBUtils;

import it.hensemberger.bean.Commenti;
import it.hensemberger.bean.Materie;

public class DaoCommenti {

	private static String QUERY_LIST="select * from Commenti";
	private static String QUERY="select * from Commenti where id= ? ";
	private static String QUERY_INSERT="INSERT INTO Commenti ( commento,id_valutazione,matricola_stud,id_commento_padre) VALUES (?,?,?,?)";
	private static String QUERY_UPDATE="UPDATE Commenti SET commento=? , id_valutazione=? , matricola_stud=? , id_commento_padre=? WHERE id=?";
	private static String QUERY_DELETE="select * from Commenti where commento=? AND matricola_stud=? ";
	
	public List<Commenti> getListCommenti() throws SQLException
	{
		Commenti com;
		List<Commenti> Listid=new ArrayList<Commenti>();
		String temp;
		Integer t;
		
		
		Connection connection=DBUtils.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement(QUERY_LIST);
		ResultSet resultSet2= preparedStatement.executeQuery();
		while(resultSet2.next())
		{
			com=new Commenti();
			t=resultSet2.getInt("id");
			com.setId(t);
			temp=resultSet2.getString("commento");
			com.setCommento(temp);
			t=resultSet2.getInt("matricola_stud");
			com.setMatricola_stud(t);
			t=resultSet2.getInt("id_valutazione");
			com.setId_valutazione(t);
			t=resultSet2.getInt("id_commento_padre");
			com.setCommento_padre(t);
			
			
			
			
			Listid.add(com);
			
			
			
		}
 
		return Listid;
		
	}
	
	
	public Commenti getCommento(Integer id) throws SQLException
	{
		Commenti com=new Commenti();
	
		String temp;
		Integer t;
		
		
		
		Connection connection=DBUtils.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement(QUERY);
		preparedStatement.setInt(1, id);
		ResultSet resultSet2= preparedStatement.executeQuery();
		if(resultSet2.next())
		{
			t=resultSet2.getInt("id");
			com.setId(t);
			temp=resultSet2.getString("commento");
			com.setCommento(temp);
			t=resultSet2.getInt("matricola_stud");
			com.setMatricola_stud(t);
			t=resultSet2.getInt("id_valutazione");
			com.setId_valutazione(t);
			t=resultSet2.getInt("id_commento_padre");
			com.setCommento_padre(t);
		}
 
		return com;
		
	}
	
	public Commenti addCommenti(Commenti co) throws SQLException
	{

	Commenti com = null;
	Integer t;

	String temp;


	//if(alreadyExist(m))

	Connection connection=DBUtils.getConnection();
	PreparedStatement preparedStatement = connection.prepareStatement(QUERY_INSERT,PreparedStatement.RETURN_GENERATED_KEYS);

	preparedStatement.setString(1,co.getCommento());
	preparedStatement.setInt(2,co.getId_valutazione());
	preparedStatement.setInt(3,co.getMatricola_stud());
	preparedStatement.setInt(4, co.getCommento_padre());
	Integer rownum = preparedStatement.executeUpdate();

	Integer id = 0;
	ResultSet rs = preparedStatement.getGeneratedKeys();
	if( rownum != 0 && rs.next()) {
	      id = rs.getInt(1);
	      com=getCommento(id);
	}


	return com;


	}
	 
	public Commenti updateCommenti(Commenti co) throws SQLException
	{
	Commenti com=new Commenti();
	Integer t;
	String temp;


	if(co==null)
		return null;

	Connection connection=DBUtils.getConnection();
	PreparedStatement preparedStatement = connection.prepareStatement(QUERY_UPDATE,PreparedStatement.RETURN_GENERATED_KEYS);

	preparedStatement.setString(1,co.getCommento());
	preparedStatement.setInt(2,co.getId_valutazione());
	preparedStatement.setInt(3,co.getMatricola_stud());
	preparedStatement.setInt(4, co.getId());
	preparedStatement.setInt(5, co.getCommento_padre());
	Integer rownum = preparedStatement.executeUpdate();


	com=getCommento(co.getId());

	return com;	
	}




	public Commenti delete(int idcom) throws SQLException{
	return null;
	}

	protected Boolean alreadyExist(Commenti co) throws SQLException
	{
	Connection connection=DBUtils.getConnection();
	PreparedStatement preparedStatement = connection.prepareStatement(QUERY_DELETE);
	preparedStatement.setString(1,co.getCommento());
	preparedStatement.setInt(2, co.getMatricola_stud());
	ResultSet rs= preparedStatement.executeQuery();

	if(rs.next())
	            return true;
	        else 
	            return false;
	}
	}
