package it.hensemberger.dao;
   


import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.hensemberger.utility.DBUtils;
import it.hensemberger.bean.Alunni;
import it.hensemberger.bean.Classe;
import it.hensemberger.bean.Indirizzo;
import it.hensemberger.bean.Materie;


public class DaoAlunni {

	private static String QUERY_LIST="select * from Alunni";
	private static String QUERY="select * from Alunni where matricola = ? ";
	private static String QUERY_INSERT="INSERT INTO Alunni ( nome,cognome,data_nascita,pass,classe) VALUES (?,?,?,?,?)";
	private static String QUERY_UPDATE="update Alunni SET  nome=?,cognome=?,data_nascita=?,pass=?,classe=? WHERE matricola=?";
	private static String QUERY_SPECIFIC="select * from Alunni where id=?";
	
	
	public List<Alunni> getListAlunni() throws SQLException
	{
		Alunni alu;
		List<Alunni> Listalu=new ArrayList<Alunni>();
		Integer t;
		String temp;
		Date data;
		

		Connection connection=DBUtils.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement(QUERY_LIST);
		ResultSet resultSet2= preparedStatement.executeQuery();
		while(resultSet2.next())
		{
			alu=new Alunni();
			
			t=resultSet2.getInt("matricola");
			alu.setMatricola(t);
			temp=resultSet2.getString("pass");
			alu.setPass(temp);
			temp=resultSet2.getString("nome");
			alu.setNome(temp);
			temp=resultSet2.getString("cognome");
			alu.setCognome(temp);
			temp=resultSet2.getString("classe");
			alu.setIdclasse(temp);
			data=resultSet2.getDate("data_nascita");
			alu.setData_nascita(data);
			
			Listalu.add(alu);
			
			
			
		}
 
		return Listalu;
		
	}
	
	
	public Alunni getAlunno(Integer id) throws SQLException
	{
		Alunni alu=new Alunni();
		Integer t;
		String temp;
		Date data;
		
		
		Connection connection=DBUtils.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement(QUERY);
		preparedStatement.setInt(1,id);
		ResultSet resultSet2= preparedStatement.executeQuery();
		if(resultSet2.next())
		{

			
			t=resultSet2.getInt("matricola");
			alu.setMatricola(t);
			temp=resultSet2.getString("pass");
			alu.setPass(temp);
			temp=resultSet2.getString("nome");
			alu.setNome(temp);
			temp=resultSet2.getString("cognome");
			alu.setCognome(temp);
			temp=resultSet2.getString("classe");
			alu.setIdclasse(temp);
			data=resultSet2.getDate("data_nascita");
			alu.setData_nascita(data);
			
		}
 
		return alu;
		
	}
	

	
	public Alunni addAlunni(Alunni a) throws SQLException
	{

	Alunni alu = null;
	Integer t;

	String temp;


	//if(alreadyExist(m))

	Connection connection=DBUtils.getConnection();
	PreparedStatement preparedStatement = connection.prepareStatement(QUERY_INSERT,PreparedStatement.RETURN_GENERATED_KEYS);
	
	preparedStatement.setString(1,a.getNome());
	preparedStatement.setString(2,a.getCognome());
	preparedStatement.setDate(3,new java.sql.Date(a.getData_nascita().getTime()));
	preparedStatement.setString(4, a.getPass());
	preparedStatement.setString(5, a.getIdclasse());//FK
	

	Integer rownum = preparedStatement.executeUpdate();

	Integer id = 0;
	ResultSet rs = preparedStatement.getGeneratedKeys();
	if( rownum != 0 && rs.next()) {
	      id = rs.getInt(1);
	      alu=getAlunno(id);
	}


	return alu;


	}
	 
	public Alunni updateAlunno(Alunni a) throws SQLException
	{
	Alunni alu=new Alunni();
	Integer t;
	String temp;


	if(a==null)
		return null;
	Connection connection=DBUtils.getConnection();
	PreparedStatement preparedStatement = connection.prepareStatement(QUERY_UPDATE,PreparedStatement.RETURN_GENERATED_KEYS);
	
	preparedStatement.setString(1,a.getNome());
	preparedStatement.setString(2,a.getCognome());
	preparedStatement.setDate(3,new java.sql.Date(a.getData_nascita().getTime()));
	preparedStatement.setString(4, a.getPass());
	preparedStatement.setString(5, a.getIdclasse());//FK
	
	preparedStatement.setInt(6, a.getMatricola());
	
	Integer rownum = preparedStatement.executeUpdate();


	alu=getAlunno(a.getMatricola());

	return alu;	
	}




	public Alunni delete(int idalu) throws SQLException{
	return null;
	}

	protected Boolean alreadyExist(Alunni a) throws SQLException
	{
	Connection connection=DBUtils.getConnection();
	PreparedStatement preparedStatement = connection.prepareStatement(QUERY_SPECIFIC);
	preparedStatement.setString(2,a.getNome());
	preparedStatement.setString(3,a.getCognome());
	ResultSet rs= preparedStatement.executeQuery();

	if(rs.next())
	            return true;
	        else 
	            return false;
	}
	}

