package it.hensemberger.dao;
   


import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.hensemberger.utility.DBUtils;

import it.hensemberger.bean.Materie;
import it.hensemberger.bean.MaterieClasse;

public class DaoMaterieClasse {

	private static String QUERY_LIST="select * from Materie_Classe";
	private static String QUERY="select * from Materie_Classe where id = ? ";
	private static String QUERY_INSERT="INSERT INTO Materie_Clesse ( id_classe,id_materia,matricola_prof,ore) VALUES (?,?,?,?,?)";
	private static String QUERY_UPDATE="update Materie_Classe SET id_classe=?,id_materia=?,matricola_prof=?,ore=? WHERE id=?";
	private static String QUERY_SPECIFIC="select * from Materie_Classe where id_classe=? AND id_materia=?  ";
	
	
	
	public List<MaterieClasse> getListMaterieClasse() throws SQLException
	{
		MaterieClasse matcla;
		List<MaterieClasse> Listmatcla=new ArrayList<MaterieClasse>();
		Integer t;
		String temp;
		
		
		
		Connection connection=DBUtils.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement(QUERY_LIST);
		ResultSet resultSet2= preparedStatement.executeQuery();
		while(resultSet2.next())
		{
			matcla  =new MaterieClasse();
			t=resultSet2.getInt("id");
			matcla.setIdesteso(t);
			t=resultSet2.getInt("ore");
			matcla.setOre(t);
			temp=resultSet2.getString("id_classe");
			matcla.setClasse(temp);
			t=resultSet2.getInt("id_materia");
			matcla.setIdmateria(t);
			t=resultSet2.getInt("matricola_prof");
			matcla.setMatrprofe(t);

			Listmatcla.add(matcla);
			
			
			
		}
 
		return Listmatcla;
		
	}
	
	
	public MaterieClasse getmateriaclasse(Integer id) throws SQLException
	{
		MaterieClasse matcla= new MaterieClasse();
	
		MaterieClasse matcla1=new MaterieClasse();
		Integer t;
		String temp;
		
		
		
		Connection connection=DBUtils.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement(QUERY);
		preparedStatement.setInt(1,id);
		ResultSet resultSet2= preparedStatement.executeQuery();
		if(resultSet2.next())
		{

			t=resultSet2.getInt("id");
			matcla1.setIdesteso(t);
			t=resultSet2.getInt("ore");
			matcla1.setOre(t);
			temp=resultSet2.getString("id_classe");
			matcla1.setClasse(temp);
			t=resultSet2.getInt("id_materia");
			matcla1.setIdmateria(t);
			t=resultSet2.getInt("matricola_prof");
			matcla1.setMatrprofe(t);

			
			
		}
 
		return matcla1;
		
	}
	
	public MaterieClasse addMaterieClasse(MaterieClasse mc) throws SQLException
	{
	MaterieClasse matcla = null;
	Integer t;

	String temp;


	//if(alreadyExist(m))

	Connection connection=DBUtils.getConnection();
	PreparedStatement preparedStatement = connection.prepareStatement(QUERY_INSERT,PreparedStatement.RETURN_GENERATED_KEYS);
	
	preparedStatement.setString(1,mc.getClasse());
	preparedStatement.setInt(2,mc.getIdmateria());
	preparedStatement.setInt(3,mc.getMatrprofe());
	preparedStatement.setInt(4,mc.getOre());

	Integer rownum = preparedStatement.executeUpdate();

	Integer id = 0;
	ResultSet rs = preparedStatement.getGeneratedKeys();
	if( rownum != 0 && rs.next()) {
	      id = rs.getInt(1);
	      matcla=getmateriaclasse(id);
	}


	return matcla;


	}
	 
	public MaterieClasse updateMaterieclasse(MaterieClasse mc) throws SQLException
	{
	MaterieClasse matcla=new MaterieClasse();
	Integer t;
	String temp;


	if(mc==null)
		return null;

	Connection connection=DBUtils.getConnection();
	PreparedStatement preparedStatement = connection.prepareStatement(QUERY_UPDATE,PreparedStatement.RETURN_GENERATED_KEYS);

	preparedStatement.setString(1,mc.getClasse());
	preparedStatement.setInt(2,mc.getIdmateria());
	preparedStatement.setInt(3,mc.getMatrprofe());
	preparedStatement.setInt(4,mc.getOre());
	preparedStatement.setInt(5,mc.getId());
	Integer rownum = preparedStatement.executeUpdate();


	matcla=getmateriaclasse(mc.getId());

	return matcla;	
	}




	public MaterieClasse delete(int idmat) throws SQLException{
	return null;
	}

	protected Boolean alreadyExist(MaterieClasse mc) throws SQLException
	{
	Connection connection=DBUtils.getConnection();
	PreparedStatement preparedStatement = connection.prepareStatement(QUERY_SPECIFIC);
	preparedStatement.setInt(1,mc.getId());
	ResultSet rs= preparedStatement.executeQuery();

	if(rs.next())
	            return true;
	        else 
	            return false;
	}
	}

















