package it.hensemberger.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mysql.jdbc.StringUtils;

import it.hensemberger.bean.Professori;
import it.hensemberger.dao.DaoProfessori;

/**
 * Servlet implementation class ServletProfessori
 */
public class ServletProfessori extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletProfessori() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String paramsString=request.getPathInfo();
		List<String>params=StringUtils.split(paramsString, "/", true);
		 DaoProfessori daoProfessori=new DaoProfessori();
		Gson gson=new Gson();
		List<Professori> ListProfessori=null;

		if(params.size()==0)
		{
			
			try{
				ListProfessori=daoProfessori.getListProfessori();
				response.setContentType("application/json);charset=UTF-8");
				response.getWriter().append(gson.toJson(ListProfessori));
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
		
		if(params.size()==1)
		{
			Professori professori;
			try{
				professori=daoProfessori.getProfessore(Integer.parseInt(params.get(0)));
				response.setContentType("application/json;charset=UTF-8");
				response.getWriter().append(gson.toJson(professori));
				if(professori==null){
					response.setStatus(HttpServletResponse.SC_NOT_FOUND);
					}else{
					}
					} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					}
					}
			
						if(params.size()>1){
						response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}
}
		

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String paramsString=request.getPathInfo();
		List<String> params=StringUtils.split(paramsString,"/",true);
		String json=getBody(request);
		Gson gson=new Gson();
		DaoProfessori daoProfessori=new DaoProfessori();
		Professori professori=gson.fromJson(json, Professori.class);
		Professori professoriGet=new Professori();
		if(params.size()==0)
		{
			try
			{
				professoriGet=daoProfessori.addProfe(professori);
				response.setContentType("application/json;charset=ITF-8");
				response.getWriter().append(gson.toJson(professoriGet));
			}catch(SQLException e)
			{
				e.printStackTrace();		
			}
		}

	}
	
	
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
	{
		String paramsString =req.getPathInfo();
		List<String> params=StringUtils.split(paramsString,"/",true);
		String json=getBody(req);
		Gson gson=new Gson();
		DaoProfessori daoProfessori=new DaoProfessori();
		Professori professori=gson.fromJson(json, Professori.class);
		Professori professoriPut=new Professori();
		if(params.size()==0)
		{
			try
			{
				professoriPut=daoProfessori.updateprofe(professori);
				resp.setContentType("application/json;charset=UTF-8");
				resp.getWriter().append(gson.toJson(professoriPut));
			}catch(SQLException e)
			{
				e.printStackTrace();
			}
		}
	}
	

	
	
	private String getBody(HttpServletRequest request) throws IOException {
		// TODO Auto-generated method stub
		  String body = null;
		    StringBuilder stringBuilder = new StringBuilder();
		    BufferedReader bufferedReader = null;

		    try {
		        InputStream inputStream = request.getInputStream();
		        if (inputStream != null) {
		            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
		            char[] charBuffer = new char[128];
		            int bytesRead = -1;
		            while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
		                stringBuilder.append(charBuffer, 0, bytesRead);
		            }
		        } else {
		            stringBuilder.append("");
		        }
		    } catch (IOException ex) {
		        throw ex;
		    } finally {
		        if (bufferedReader != null) {
		            try {
		                bufferedReader.close();
		            } catch (IOException ex) {
		                throw ex;
		            }
		        }
		    }

		    body = stringBuilder.toString();
		    return body;
		}

}







 





