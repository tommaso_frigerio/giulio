package it.hensemberger.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mysql.jdbc.StringUtils;

import it.hensemberger.bean.Commenti;
import it.hensemberger.dao.DaoCommenti;

/**
 * Servlet implementation class ServletCommenti
 */
public class ServletCommenti extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletCommenti() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String paramsString=request.getPathInfo();
		List<String>params=StringUtils.split(paramsString, "/", true);
		 DaoCommenti daoCommenti=new DaoCommenti();
		Gson gson=new Gson();
		List<Commenti> ListCommenti=null;

		if(params.size()==0)
		{
			
			try{
				ListCommenti=daoCommenti.getListCommenti();
				response.setContentType("application/json);charset=UTF-8");
				response.getWriter().append(gson.toJson(ListCommenti));
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
		
		if(params.size()==1)
		{
			Commenti commenti;
			try{
				commenti=daoCommenti.getCommento(Integer.parseInt(params.get(0)));
				response.setContentType("application/json;charset=UTF-8");
				response.getWriter().append(gson.toJson(commenti));
				if(commenti==null){
					response.setStatus(HttpServletResponse.SC_NOT_FOUND);
					}else{
					}
					} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					}
					}
			
						if(params.size()>1){
						response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}
}
		

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String paramsString=request.getPathInfo();
		List<String> params=StringUtils.split(paramsString,"/",true);
		String json=getBody(request);
		Gson gson=new Gson();
		DaoCommenti daoCommenti=new DaoCommenti();
		Commenti commenti=gson.fromJson(json, Commenti.class);
		Commenti commentiGet=new Commenti();
		if(params.size()==0)
		{
			try
			{
				commentiGet=daoCommenti.addCommenti(commenti);
				response.setContentType("application/json;charset=ITF-8");
				response.getWriter().append(gson.toJson(commentiGet));
			}catch(SQLException e)
			{
				e.printStackTrace();		
			}
		}

	}
	
	
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
	{
		String paramsString =req.getPathInfo();
		List<String> params=StringUtils.split(paramsString,"/",true);
		String json=getBody(req);
		Gson gson=new Gson();
		DaoCommenti daoCommenti=new DaoCommenti();
		Commenti commenti=gson.fromJson(json,Commenti.class);
		Commenti commentiPut=new Commenti();
		if(params.size()==0)
		{
			try
			{
				commentiPut=daoCommenti.updateCommenti(commenti);
				resp.setContentType("application/json;charset=UTF-8");
				resp.getWriter().append(gson.toJson(commentiPut));
			}catch(SQLException e)
			{
				e.printStackTrace();
			}
		}
	}
	

	
	
	private String getBody(HttpServletRequest request) throws IOException {
		// TODO Auto-generated method stub
		  String body = null;
		    StringBuilder stringBuilder = new StringBuilder();
		    BufferedReader bufferedReader = null;

		    try {
		        InputStream inputStream = request.getInputStream();
		        if (inputStream != null) {
		            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
		            char[] charBuffer = new char[128];
		            int bytesRead = -1;
		            while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
		                stringBuilder.append(charBuffer, 0, bytesRead);
		            }
		        } else {
		            stringBuilder.append("");
		        }
		    } catch (IOException ex) {
		        throw ex;
		    } finally {
		        if (bufferedReader != null) {
		            try {
		                bufferedReader.close();
		            } catch (IOException ex) {
		                throw ex;
		            }
		        }
		    }

		    body = stringBuilder.toString();
		    return body;
		}

}